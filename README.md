# Giddy.io

This is the Giddy.io Web Platform -- Version 2 repository. This has been built utilizing the best that React has to offer. We heavily use ES6 within the code where possible.

We utilize a lot of great packages/concepts in this project, some of the core ones are listed below:

* [React](https://facebook.github.io/react/)
* [React Router](https://github.com/reactjs/react-router)
* [Redux](https://github.com/reactjs/redux)
* [redux-thunk](https://github.com/gaearon/redux-thunk)
* [Redux Devtools Extension](https://github.com/zalmoxisus/redux-devtools-extension)
* [Immutable-js](https://facebook.github.io/immutable-js/)
* [Webpack 2](https://gist.github.com/sokra/27b24881210b56bbaff7)
* [ESLint](http://eslint.org/) ([Airbnb style](https://github.com/airbnb/javascript)).
* [CSS Modules](https://github.com/css-Modules/css-Modules)
* [StyleLint](http://stylelint.io/)
* [Express](https://expressjs.com/)
* [Babel](https://babeljs.io/)
* [Redux Form](http://redux-form.com/6.1.0/)
* [Grommet](https://grommet.github.io/) - for UI. Temporary, will be removing.

There are of course a lot more packages included. They will be listed as deemed necessary.

One note: If you get the bug below on first start, try **npm run build** in terminal

> webpack-isomorphic-tools (waiting for the first webpack build to finish)
