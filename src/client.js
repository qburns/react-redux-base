import React from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
import { Provider } from 'react-redux';
import { syncHistoryWithStore } from 'react-router-redux';
import { fromJS } from 'immutable';
import { AppContainer } from 'react-hot-loader';
import configureStore from './redux/store';
/* eslint-disable*/
//We're importing browserHistory separately as we're going to be using it in the middleware on the server side and we can't use es6
import { match, Router, browserHistory } from 'react-router';
/*eslint-enable*/

// redux-immutable only allow immutable obj
const initialState = fromJS(window.__INITIAL_STATE__);
const store = configureStore(initialState);
const history = syncHistoryWithStore(browserHistory, store, {
  selectLocationState: state => state.get('routing').toJS(),
});
const mountNode = document.getElementById('react-view');

const renderApp = () => {
  const routes = require('./routes').default(store);

  // Sync routes both on client and server
  match({ history: browserHistory, routes }, (error, redirectLocation, renderProps) => {
    // Using the enhanced history of react-redux-router instead of the 'browserHistory'
    const props = Object.assign({}, renderProps, { history });

/*eslint-disable */
    render(
      <AppContainer>
      <Provider store={store}>
        <Router {...props} />
      </Provider>
      </AppContainer>,
      mountNode
    );
  });
};
/*eslint-enable */

// Enable hot reload by react-hot-loader
if (module.hot) {
  const reRenderApp = () => {
    try {
      renderApp();
    } catch (error) {
      const RedBox = require('redbox-react').default;

      render(<RedBox error={error} />, mountNode);
    }
  };

  module.hot.accept('./routes', () => {
    setImmediate(() => {
      // Preventing the hot reloading error from react-router
      unmountComponentAtNode(mountNode);
      reRenderApp();
    });
  });
}

renderApp();
