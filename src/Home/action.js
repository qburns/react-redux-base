export const HAVE_ALL_DATA = 'HAVE_ALL_DATA';

// Export this function for testing
export const fetchData = () => (dispatch) => {
  // Will need to do some stuff here to grab hot ideas/projects etc for homepage
  dispatch({ type: HAVE_ALL_DATA });
};

/* istanbul ignore next */
const shouldFetchData = (state) => {
  // In development, we will allow action dispatching
  // or your reducer hot reloading won't updated on the view
  /* istanbul ignore next */
  if (__DEV__) return true;

  /* istanbul ignore next */
  const home = state.get('home');

  /* istanbul ignore next */
  if (home.get('readyState') === HAVE_ALL_DATA) return false; // Preventing double fetching data

  /* istanbul ignore next */
  return true;
};

/* istanbul ignore next */
export const fetchDataIfNeeded = () => (dispatch, getState) => {
  if (shouldFetchData(getState())) {
    /* istanbul ignore next */
    return dispatch(fetchData());
  }

  /* istanbul ignore next */
  return null;
};
