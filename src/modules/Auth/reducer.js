/* eslint new-cap:0 */

// Global app state

import { Map } from 'immutable';

/*eslint-disable*/
import {
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGIN_ATTEMPT,
  LOGOUT_USER,
  AUTO_RETRIEVE_CODE_SUCCESS,
  RETRIEVE_CODE_SUCCESS,
  GET_LOGGED_IN_USER_SUCCESS,
  GET_USER_FROM_COOKIE_SUCCESS
} from './action';
/*eslint-enable*/

const initialState = Map({
});

export default (state = initialState, action) => {
  switch (action.type) {
    case AUTO_RETRIEVE_CODE_SUCCESS:
      return state.merge({
        code: action.payload.code,
      });
    case RETRIEVE_CODE_SUCCESS:
      return state.merge({
        code: action.payload.code,
      });
    case LOGIN_ATTEMPT:
      return state.merge({
        isAuthenticating: true,
        statusText: null,
      });
    case LOGIN_SUCCESS:
      return state.merge({
        isAuthenticating: false,
        isAuthenticated: true,
        token: action.payload,
        statusText: 'You have been successfully logged in.',
      });
    case LOGIN_FAILURE:
      return state.merge({
        isAuthenticating: false,
        isAuthenticated: false,
        statusText: `Authentication Error: ${action.err}`,
      });
    case GET_LOGGED_IN_USER_SUCCESS:
      return state.merge({
        userName: action.payload.username,
        id: action.payload.id,
        coins: action.payload.coins,
        avatar: action.payload.avatar,
        statusText: 'Successfully retrieved user profile.',
      });
    case GET_USER_FROM_COOKIE_SUCCESS:
      return state.merge({
        userName: action.payload.username,
        id: action.payload.id,
        coins: action.payload.coins,
        avatar: action.payload.avatar,
        statusText: 'Successfully retrieved user profile.',
      });
    case LOGOUT_USER:
      return initialState;
    default:
      return state;
  }
};
