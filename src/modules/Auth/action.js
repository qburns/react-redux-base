import cookie from 'react-cookie';
import { push } from 'react-router-redux';
import { UserAuthWrapper } from 'redux-auth-wrapper';

import Giddy from '../../config';


export const AUTO_RETRIEVE_CODE = 'AUTO_RETRIEVE_CODE';
export const AUTO_RETRIEVE_CODE_ATTEMPT = 'AUTO_RETRIEVE_CODE_ATTEMPT';
export const AUTO_RETRIEVE_CODE_SUCCESS = 'AUTO_RETRIEVE_CODE_SUCCESS';
export const AUTO_RETRIEVE_CODE_FAILURE = 'AUTO_RETRIEVE_CODE_FAILURE';

export const RETRIEVE_CODE_ATTEMPT = 'RETRIEVE_CODE_ATTEMPT';
export const RETRIEVE_CODE_SUCCESS = 'RETRIEVE_CODE_SUCCESS';
export const RETRIEVE_CODE_FAILURE = 'RETRIEVE_CODE_FAILURE';

export const LOGIN_ATTEMPT = 'LOGIN_ATTEMPT';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const LOGOUT_USER = 'LOGOUT_USER';
export const NOT_LOGGED_IN = 'NOT_LOGGED_IN';
export const USER_WAS_REDIRECTED = 'USER_WAS_REDIRECTED';

export const GET_LOGGED_IN_USER_ATTEMPT = 'GET_LOGGED_IN_USER_ATTEMPT';
export const GET_LOGGED_IN_USER_SUCCESS = 'GET_LOGGED_IN_USER_SUCCESS';
export const GET_LOGGED_IN_USER_FAILURE = 'GET_LOGGED_IN_USER_FAILURE';

export const GET_USER_FROM_COOKIE_ATTEMPT = 'GET_USER_FROM_COOKIE_ATTEMPT';
export const GET_USER_FROM_COOKIE_SUCCESS = 'GET_USER_FROM_COOKIE_SUCCESS';



// User has either logged in with username from login form or has just registered an account
// and is logging in for the first time
export const retrieveCodeWithUsername = userInfo => (dispatch) => {
  dispatch({ type: RETRIEVE_CODE_ATTEMPT });

  const userObj = { username: userInfo.get('username') };

  return Giddy.SDK.loginWithUsername(userObj)
    .then((res) => {
      dispatch({ type: RETRIEVE_CODE_SUCCESS, payload: {code: res.data.code} });
    }).catch((err) => {
      dispatch({ type: RETRIEVE_CODE_FAILURE, err: err.response.data });
    });
};


// User has successfully logged in with the secret code and pin
// and now we need to get their profile details
export const getLoggedInUserProfile = token => (dispatch) => {
  // Make sure we set a realistic expiration here in future
  const expiration = new Date(Date.now() + 7 * 24 * 60 * 60 * 1000);

  dispatch({ type: GET_LOGGED_IN_USER_ATTEMPT });

  return Giddy.SDK.withAuthorizationToken(token).getProfile({ username: 'me' })
    .then((res) => {
      const stringifiedCookie = JSON.stringify(res.data);
      cookie.save('userInfo', stringifiedCookie, {expires: expiration});
      dispatch({ type: GET_LOGGED_IN_USER_SUCCESS, payload: res.data });
    })
    .catch((err) => {
      dispatch({ type: GET_LOGGED_IN_USER_FAILURE, err: err.response.data });
    });
};


// Redirecting the user based on the parameter. Will redirect to home if nothing is passed.
export const redirectUser = (redirectUrl = '/') => (dispatch) => {
  dispatch(push(redirectUrl));
};

export const logoutUser = () => (dispatch) => {
  cookie.remove('userInfo');
  cookie.remove('token');
  dispatch({ type: LOGOUT_USER });
};


// Used to save auth token
const saveAuthToken = (token) => {
  // Make sure we set a realistic expiration here in future
  const expiration = new Date(Date.now() + 7 * 24 * 60 * 60 * 1000);
  cookie.save('token', token, {expires: expiration });
};



// User has entered a Pin code from the pin form and we are sending pin + code
export const attemptLogin = loginInfo => (dispatch) => {
  dispatch({ type: LOGIN_ATTEMPT, loginInfo });

  return Giddy.SDK.loginWithCodeAndPin(
    { code: loginInfo.code,
      pin: loginInfo.pin,
    }).then((res) => {
      if (res.data.token) {
        dispatch({ type: LOGIN_SUCCESS, payload: res.data.token });
        dispatch(getLoggedInUserProfile(res.data.token));
        saveAuthToken(res.data.token);
      } else {
        console.log('Server 200 but no token returned');
      }
    }).catch((err) => {
      dispatch({ type: LOGIN_FAILURE, err: err.response.data });
    });
};

// User has submittted pin form. Grabbing values and passing to attemptLogin
export const loginFromPinPage = loginInfo => (dispatch, getState) => {
  const state = getState();
  const user = state.get('user');
  const pin = loginInfo.get('pin');
  const accessCode = user.get('code');
  const loginObj = {code: accessCode, pin: pin, };
  dispatch(attemptLogin(loginObj));
};


// Checking to see if the user has a cookie with his info saved when they first visit application
export const getUserFromCookie = (dispatch) => {
  dispatch({ type: GET_USER_FROM_COOKIE_ATTEMPT });
  const userCookie = cookie.load('userInfo');
  if (userCookie) {
    dispatch({ type: GET_USER_FROM_COOKIE_SUCCESS, payload: userCookie });
  }
};

/* eslint-disable */
// Redirects to /login by default if authentication is required and user is not logged in
export const authenticationRequired = UserAuthWrapper({
  authSelector: state => ( state.get('user').toJS() ),
  redirectAction: push,
  wrapperDisplayName: 'authenticationRequired'
});

/* eslint-enable */
