import chalk from 'chalk';
import { injectReducer } from './redux/reducers';
import App from './containers/App';
import { authenticationRequired } from './modules/Auth/action';

const errorLoading = (err) => {
  console.error(chalk.red(`==> 😭  Dynamic page loading failed ${err}`));
};

const loadModule = cb => (Component) => {
  cb(null, Component.default);
};

export default function createRoutes(store) {
  const connect = fn => (nextState, replaceState) => fn(store, nextState, replaceState);

  return {
    path: '/',
    component: App,
    indexRoute: {
      getComponent(location, cb) {
        const importModules = Promise.all([
          System.import('./containers/Home'),
          System.import('./containers/Home/reducer'),
          System.import('./modules/Auth/reducer'),
        ]);

        const renderRoute = loadModule(cb);

        importModules
          .then(([Component, reducer, userReducer]) => {
            injectReducer(store, 'home', reducer.default);
            injectReducer(store, 'user', userReducer.default);

            renderRoute(Component);
          })
          .catch(errorLoading);
      },
    },
    childRoutes: [
      {
        onEnter: connect(authenticationRequired.onEnter),
        path: 'protected',
        getComponent(location, cb) {
          const importModules = Promise.all([
            System.import('./containers/ExampleProtectedPage'),
            System.import('./modules/Auth/reducer'),
          ]);

          const renderRoute = loadModule(cb);

          importModules
            .then(([Component, userReducer]) => {
              injectReducer(store, 'user', userReducer.default);

              renderRoute(Component);
            })
            .catch(errorLoading);
        },
      },
      {
        path: 'login',
        getComponent(location, cb) {
          const importModules = Promise.all([
            System.import('./containers/LoginPage'),
            System.import('./containers/LoginPage/reducer'),
            System.import('./modules/Auth/reducer'),
          ]);

          const renderRoute = loadModule(cb);

          importModules
            .then(([Component, reducer, userReducer]) => {
              injectReducer(store, 'loginPage', reducer.default);
              injectReducer(store, 'user', userReducer.default);

              renderRoute(Component);
            })
            .catch(errorLoading);
        },
      },
      {
        path: 'signup',
        getComponent(location, cb) {
          const importModules = Promise.all([
            System.import('./containers/SignupPage'),
            System.import('./containers/SignupPage/reducer'),
            System.import('./modules/Auth/reducer'),
          ]);

          const renderRoute = loadModule(cb);

          importModules
            .then(([Component, reducer, userReducer]) => {
              injectReducer(store, 'signupPage', reducer.default);
              injectReducer(store, 'user', userReducer.default);

              renderRoute(Component);
            })
            .catch(errorLoading);
        },
      },
      {
        path: '*',
        getComponent(location, cb) {
          System.import('./containers/NotFound')
            .then(loadModule(cb))
            .catch(errorLoading);
        },
      },
    ],
  };
}
