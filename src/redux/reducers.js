/* eslint new-cap:0 */

import { combineReducers } from 'redux-immutable';
import { Map, fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form/immutable';

// Initial routing state
const routeInitialState = fromJS({
  locationBeforeTransitions: null,
});

// Merge route into the global application state (react-router-redux + immutable)
const routing = (state = routeInitialState, action) => {
  if (action.type === LOCATION_CHANGE) {
    return state.merge({
      locationBeforeTransitions: action.payload,
    });
  }

  return state;
};

export default function createReducer(asyncReducers) {
  return combineReducers({
    routing,
    // Make sure we register all async reducers here, otherwise Redux will throw a warning
    user: (state = Map({})) => state,
    home: (state = Map({})) => state,
    userInfo: (state = Map({})) => state,
    signupPage: (state = Map({})) => state,
    loginPage: (state = Map({})) => state,
    form: formReducer,
    ...asyncReducers,
  });
}


/* eslint-disable */
// Inject the async reducers into our global redux store
export const injectReducer = (store, name, reducer) => {
  store.asyncReducers[name] = reducer
  store.replaceReducer(createReducer(store.asyncReducers))
}
/* eslint-enable */
