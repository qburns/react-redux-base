//This configuration file will take our existing default config
// and overwrite with production settings

const Giddy = require('../sdk/giddy.js');
const defaultConfig = require('./default');

const prodDomain = 'firstbuild-stg.herokuapp.com';
const prodSDK = new Giddy.Giddy().withStagingEnvironment();

const config = {
  DOMAIN: prodDomain,
  BASE_URL: 'https://' + prodDomain,
  SDK: prodSDK,
};

module.exports = Object.assign({}, defaultConfig, config);
