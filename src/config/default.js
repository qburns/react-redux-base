const domain = 'firstbuild-dev.herokuapp.com';
const Giddy = require('../sdk/giddy.js');
const giddySDK = new Giddy.Giddy().withDevelopmentEnvironment();

module.exports = {
  host: process.env.HOST || 'localhost',
  port: process.env.PORT,
  DOMAIN: domain,
  BASE_URL: 'https://' + domain,
  SDK: giddySDK,
  app: {
    htmlAttributes: { lang: 'en' },
    title: 'Giddy.io',
    titleTemplate: 'Giddy.io - %s',
    meta: [
      { name: 'description', content: 'Giddy.io -- Need a meta here' },
    ],
  },
};
