
export const signupValidation = (values) => {

  const username = values.get('username');
  const email = values.get('email');
  const alphanumericRegex = /^[a-z0-9]+$/i;
  const emailRegex = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
  const errors = {};
  if (!username) {
    errors.username = 'Required';
    // eslint-disable-next-line
  } else if (!alphanumericRegex.test(username) || !typeof username == 'string') {
    errors.username = 'Must contain only letters and numbers';
  } else if (username.length >= 30) {
    errors.username = 'Cannot be over 30 characters';
  }
  if (!email) {
    errors.email = 'Required';
  } else if (!emailRegex.test(email)) {
    errors.email = 'Valid email address required.';
  }
  return errors;
};
