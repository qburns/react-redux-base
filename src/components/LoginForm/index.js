import React, { Component, PropTypes } from 'react';
import { Field, reduxForm } from 'redux-form/immutable';

import Section from 'grommet/components/Section';
import Form from 'grommet/components/Form';
import FormFields from 'grommet/components/FormFields';
import FormField from 'grommet/components/FormField';
import Footer from 'grommet/components/Footer';
import Header from 'grommet/components/Header';
import Title from 'grommet/components/Title';
import Box from 'grommet/components/Box';
import Notification from 'grommet/components/Notification';
import { loginValidation as validate } from './validate';
import styles from './LoginForm.scss';

const showError = (error) => {
  if (error) {
    const errName = error.get('name');
    let message = `We\'ve encountered an error. Error code: ${errName}`;
    if (errName === 'UsernameNotFoundError') {
      message = 'We couldn\'t find a profile associated with this username.';
    } else if (errName === 'EmailAddressNotAvailableError') {
      message = 'There is already an account associated with this email. Did you intend to login instead?';
    }
    return <Notification status="critical" message={message} size="large" />;
  }
};

const renderField = ({ input, label, type, meta: { touched, error } }) => {
  return (
    <FormField label={label} error={touched && error ? error : ''}>
      <input  {...input} type={type} />
    </FormField>
  );
};


const LoginForm = (props) => {
  const { handleSubmit, dirty, invalid, submitting, serverError } = props;

  const disableButtonCheck = submitting || invalid || (serverError && invalid);

  let buttonDisabled = '';
  if (disableButtonCheck) {
    buttonDisabled = 'grommetux-button--disabled';
  }

  const buttonClass = `grommetux-button--primary grommetux-button ${buttonDisabled}`;


  return (
    <Section align="center" justify="center" className={styles.LoginForm}>
      <Header align="center" justify="center"><Title>Login to Giddy</Title></Header>
      <Box align="center" justify="center">
        <div id="error-area">
          {showError(serverError)}
        </div>
        <Form onSubmit={handleSubmit}>
          <FormFields>
            <fieldset>
              <Field name="username" type="text" component={renderField} label="Username" />
            </fieldset>
          </FormFields>
          <Footer align="center" justify="center" pad={{ vertical: 'medium' }}>
            <button className={buttonClass} type="submit" disabled={disableButtonCheck}>{submitting ? 'Waiting...' : 'Continue'}</button>
          </Footer>
        </Form>
      </Box>
    </Section>
    );
};

export default reduxForm({
  form: 'rf-LoginForm', // a unique identifier for this form
  validate,
})(LoginForm);
