export const loginValidation = (values) => {

  const username = values.get('username');
  const alphanumericRegex = /^[a-z0-9]+$/i;
  const errors = {};
  if (!username) {
    errors.username = 'Required';
  } else if (!alphanumericRegex.test(username) || typeof username !== 'string') {
    errors.username = 'Usernames contain only letters and numbers';
  } else if (username.length >= 30) {
    errors.username = 'Usernames cannot be over 30 characters';
  }
  return errors;
};
