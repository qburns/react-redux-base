export const pinValidation = (values) => {

  const pin = values.get('pin');
  var numberReg = /^\d+$/;
  const errors = {};
  if (!pin) {
    errors.pin = 'Required';
  } else if (!numberReg.test(pin)) {
    errors.pin = 'Pin consists of only numbers.';
  } else if (pin.length !== 6) {
    errors.pin = 'Pin must be exactly 6 digits.';
  } // check if is a numerical value
  return errors;
};
