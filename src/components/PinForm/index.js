import React, { Component, PropTypes } from 'react';
import { Field, reduxForm } from 'redux-form/immutable';

import Section from 'grommet/components/Section';
import Form from 'grommet/components/Form';
import FormFields from 'grommet/components/FormFields';
import FormField from 'grommet/components/FormField';
import Footer from 'grommet/components/Footer';
import Header from 'grommet/components/Header';
import Title from 'grommet/components/Title';
import Paragraph from 'grommet/components/Paragraph';
import Box from 'grommet/components/Box';
import Notification from 'grommet/components/Notification';
import { pinValidation as validate } from './validate';
import styles from './PinForm.scss';

const showError = (error) => {
  if (error) {
    const errName = error.get('name');
    let message = `We\'ve encountered an error. Error code: ${errName}`;
    if (errName === 'AuthorizationError') {
      message = 'You\'ve entered an incorrect pin code. Try again.';
    } else if (errName === 'InputValidationError') {
      message = 'Pin code must be exactly 6 numeric digits.';
    }
    return <Notification status="critical" message={message} size="large" />;
  }
};

const renderField = ({ input, label, type, meta: { touched, error } }) => {
  return (
    <FormField label={label} error={touched && error ? error : ''}>
      <input  {...input} type={type} />
    </FormField>
  );
};


const PinForm = (props) => {
  const { handleSubmit, invalid, submitting, serverError, userEmail } = props;

  const disableButtonCheck = submitting || invalid || (serverError && invalid);

  let buttonDisabled = '';
  if (disableButtonCheck) {
    buttonDisabled = 'grommetux-button--disabled';
  }

  const buttonClass = `grommetux-button--primary grommetux-button ${buttonDisabled}`;

  const emailMessage = userEmail ? userEmail : 'you';

  return (
    <Section align="center" justify="center" className={styles.PinForm}>
      <Header size="full" align="center" justify="center">
          <Title>Input Pin</Title>
      </Header>
      <Box pad="none" size="full" align="center" justify="center">
        <Paragraph align="center">We've just dispatched an email to {emailMessage} containing a pin code, enter it below.</Paragraph>
      </Box>
      <Box align="center" justify="center">
        <div id="error-area">
          {showError(serverError)}
        </div>
        <Form onSubmit={handleSubmit}>
          <FormFields>
            <fieldset>
              <Field name="pin" type="text" component={renderField} label="Pin" />
            </fieldset>
          </FormFields>
          <Footer align="center" justify="center" pad={{ vertical: 'medium' }}>
            <button className={buttonClass} type="submit" disabled={disableButtonCheck}>{submitting ? 'Logging in...' : 'Login'}</button>
          </Footer>
        </Form>
      </Box>
    </Section>
    );
};

export default reduxForm({
  form: 'rf-PinForm', // a unique identifier for this form
  validate,
})(PinForm);
