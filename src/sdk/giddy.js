/**
 * Copyright (c) 2016 FirstBuild <support@firstbuild.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/*eslint-disable */
import axios from 'axios';

(function () {
  var Giddy = this.Giddy = function () {

  };

  Giddy.prototype.withEndpoint = function (endpoint) {
    this.endpoint = endpoint;
    return this;
  };

  Giddy.prototype.withAuthorizationToken = function (token) {
    this.token = token;
    return this;
  };

  Giddy.prototype.withDevelopmentEnvironment = function () {
    return this.withEndpoint('https://firstbuild-dev.herokuapp.com');
  };

  Giddy.prototype.withStagingEnvironment = function () {
    return this.withEndpoint('https://firstbuild-stg.herokuapp.com');
  };

  Giddy.prototype.withProductionEnvironment = function () {
    return this.withEndpoint('https://firstbuild.herokuapp.com');
  };

  Giddy.prototype.getAvailableEmailAddress = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/email/available' + '?email=' + encodeURIComponent(options.email),
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.getRegisteredEmailAddress = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/email/registered' + '?email=' + encodeURIComponent(options.email),
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.getValidEmailAddress = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/email/valid' + '?email=' + encodeURIComponent(options.email),
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.getRandomId = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/id/random',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.acceptInvitation = function (options) {
    return axios({
      method: 'POST',
      url: this.endpoint + '/v1/invitations/accept',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        code: options.code
      })
    });
  };

  Giddy.prototype.loginWithEmailAddress = function (options) {
    return axios({
      method: 'POST',
      url: this.endpoint + '/v1/login/email',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        email: options.email
      })
    });
  };

  Giddy.prototype.loginWithCodeAndPin = function (options) {
    return axios({
      method: 'POST',
      url: this.endpoint + '/v1/login/pin',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        code: options.code,
        pin: options.pin
      })
    });
  };

  Giddy.prototype.loginWithUsername = function (options) {
    return axios({
      method: 'POST',
      url: this.endpoint + '/v1/login/username',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        username: options.username
      })
    });
  };

  Giddy.prototype.getValidPin = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/pin/valid' + '?pin=' + encodeURIComponent(options.pin),
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.getPrototypesByUpdates = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/prototypes/by/updates',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.registerWithEmailAddress = function (options) {
    return axios({
      method: 'POST',
      url: this.endpoint + '/v1/register/email',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        username: options.username,
        email: options.email
      })
    });
  };

  Giddy.prototype.getStatus = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/status',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.createTag = function (options) {
    return axios({
      method: 'POST',
      url: this.endpoint + '/v1/tags',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        title: options.title
      })
    });
  };

  Giddy.prototype.getTags = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/tags',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.searchTags = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/tags/search' + '?q=' + encodeURIComponent(options.q),
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.getTasks = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/tasks',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.getUpdates = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/updates',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.getAvailableUsername = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/username/available' + '?username=' + encodeURIComponent(options.username),
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.getRegisteredUsername = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/username/registered' + '?username=' + encodeURIComponent(options.username),
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.getValidUsername = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/username/valid' + '?username=' + encodeURIComponent(options.username),
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.getUsers = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/users',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.updateProfile = function (options) {
    return axios({
      method: 'PATCH',
      url: this.endpoint + '/v1/users/' + options.username,
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        name: options.name,
        company: options.company,
        city: options.city,
        state: options.state,
        zip: options.zip,
        country: options.country,
        email: options.email,
        avatar: options.avatar
      })
    });
  };

  Giddy.prototype.deleteUser = function (options) {
    return axios({
      method: 'DELETE',
      url: this.endpoint + '/v1/users/' + options.username,
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.getProfile = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/users/' + options.username,
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.getBookmarks = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/users/' + options.username + '/bookmarks',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.createBookmark = function (options) {
    return axios({
      method: 'POST',
      url: this.endpoint + '/v1/users/' + options.username + '/bookmarks',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        url: options.url
      })
    });
  };

  Giddy.prototype.getPrototypes = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/users/' + options.username + '/prototypes',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.createPrototype = function (options) {
    return axios({
      method: 'POST',
      url: this.endpoint + '/v1/users/' + options.username + '/prototypes',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        url: options.url
      })
    });
  };

  Giddy.prototype.updatePrototype = function (options) {
    return axios({
      method: 'PATCH',
      url: this.endpoint + '/v1/users/' + options.username + '/prototypes/' + options.prototype,
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        title: options.title,
        description: options.description,
        image: options.image
      })
    });
  };

  Giddy.prototype.getTeamForPrototype = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/users/' + options.username + '/prototypes/' + options.prototype + '/team',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.deletePrototype = function (options) {
    return axios({
      method: 'DELETE',
      url: this.endpoint + '/v1/users/' + options.username + '/prototypes/' + options.prototype,
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.getPrototype = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/users/' + options.username + '/prototypes/' + options.prototype,
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.unstarPrototype = function (options) {
    return axios({
      method: 'DELETE',
      url: this.endpoint + '/v1/users/' + options.username + '/prototypes/' + options.prototype + '/stars',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.starPrototype = function (options) {
    return axios({
      method: 'POST',
      url: this.endpoint + '/v1/users/' + options.username + '/prototypes/' + options.prototype + '/stars',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.getPrototypeStars = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/users/' + options.username + '/prototypes/' + options.prototype + '/stars',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.getPrototypeTags = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/users/' + options.username + '/prototypes/' + options.prototype + '/tags',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.getPrototypeTasks = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/users/' + options.username + '/prototypes/' + options.prototype + '/tasks',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.createPrototypeTask = function (options) {
    return axios({
      method: 'POST',
      url: this.endpoint + '/v1/users/' + options.username + '/prototypes/' + options.prototype + '/tasks',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        title: options.title,
        description: options.description,
        estimated: options.estimated,
        tags: options.tags
      })
    });
  };

  Giddy.prototype.updatePrototypeTask = function (options) {
    return axios({
      method: 'PATCH',
      url: this.endpoint + '/v1/users/' + options.username + '/prototypes/' + options.prototype + '/tasks/' + options.task,
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        title: options.title,
        description: options.description,
        estimated: options.estimated,
        worked: options.worked,
        tags: options.tags,
        status: options.status,
        assigned: options.assigned
      })
    });
  };

  Giddy.prototype.getPrototypeUpdates = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/users/' + options.username + '/prototypes/' + options.prototype + '/updates',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.createPrototypeUpdate = function (options) {
    return axios({
      method: 'POST',
      url: this.endpoint + '/v1/users/' + options.username + '/prototypes/' + options.prototype + '/updates',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        title: options.title,
        description: options.description,
        tags: options.tags,
        uploads: options.uploads
      })
    });
  };

  Giddy.prototype.getPrototypeUpdate = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/users/' + options.username + '/prototypes/' + options.prototype + '/updates/' + options.update,
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.deletePrototypeUpdate = function (options) {
    return axios({
      method: 'DELETE',
      url: this.endpoint + '/v1/users/' + options.username + '/prototypes/' + options.prototype + '/updates/' + options.update,
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.createQuestion = function (options) {
    return axios({
      method: 'POST',
      url: this.endpoint + '/v1/users/' + options.username + '/questions',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        title: options.title,
        question: options.question,
        answer: options.answer,
        choices: options.choices,
        tags: options.tags
      })
    });
  };

  Giddy.prototype.getUnansweredQuestions = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/users/' + options.username + '/questions',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.createQuestionAnswer = function (options) {
    return axios({
      method: 'POST',
      url: this.endpoint + '/v1/users/' + options.username + '/questions/' + options.question,
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        answer: options.answer
      })
    });
  };

  Giddy.prototype.skipQuestionAnswer = function (options) {
    return axios({
      method: 'POST',
      url: this.endpoint + '/v1/users/' + options.username + '/questions/' + options.question + '/skip',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.getUploads = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/users/' + options.username + '/uploads',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.createUpload = function (options) {
    return axios({
      method: 'POST',
      url: this.endpoint + '/v1/users/' + options.username + '/uploads',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'multipart/form-data'
      },
      data: JSON.stringify({
        file: options.file,
        description: options.description
      })
    });
  };

  Giddy.prototype.getUpload = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/users/' + options.username + '/uploads/' + options.upload,
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.getUploadFile = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/users/' + options.username + '/uploads/' + options.upload + '/raw',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.getUsersByTag = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/users/by/tag',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.getUsersByUpdates = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/users/by/updates',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.createFeedback = function (options) {
    return axios({
      method: 'POST',
      url: this.endpoint + '/v1/users/me/feedback',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        title: options.title,
        description: options.description,
        uploads: options.uploads
      })
    });
  };

  Giddy.prototype.addTeamMember = function (options) {
    return axios({
      method: 'POST',
      url: this.endpoint + '/v1/users/me/prototypes/' + options.prototype + '/team',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.createPrototypeTag = function (options) {
    return axios({
      method: 'POST',
      url: this.endpoint + '/v1/users/me/prototypes/' + options.prototype + '/tags',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        id: options.id
      })
    });
  };

  Giddy.prototype.startTask = function (options) {
    return axios({
      method: 'POST',
      url: this.endpoint + '/v1/users/me/prototypes/' + options.prototype + '/tasks/' + options.task + '/start',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.stopTask = function (options) {
    return axios({
      method: 'POST',
      url: this.endpoint + '/v1/users/me/prototypes/' + options.prototype + '/tasks/' + options.task + '/stop',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.updatePrototypeUpdate = function (options) {
    return axios({
      method: 'PATCH',
      url: this.endpoint + '/v1/users/me/prototypes/' + options.prototype + '/updates/' + options.update,
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        title: options.title,
        description: options.description,
        tags: options.tags,
        uploads: options.uploads
      })
    });
  };

  Giddy.prototype.getAnsweredQuestions = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/users/me/questions/answered',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.getCreatedQuestions = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/users/me/questions/created',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.acceptTagsForUpdate = function (options) {
    return axios({
      method: 'POST',
      url: this.endpoint + '/v1/users/me/tags/accept',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        update: options.update
      })
    });
  };

  Giddy.prototype.rejectTagsForUpdate = function (options) {
    return axios({
      method: 'POST',
      url: this.endpoint + '/v1/users/me/tags/reject',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        update: options.update
      })
    });
  };

  Giddy.prototype.skipTagsForUpdate = function (options) {
    return axios({
      method: 'POST',
      url: this.endpoint + '/v1/users/me/tags/skip',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        update: options.update
      })
    });
  };

  Giddy.prototype.getUnverifiedTags = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/users/me/tags/unverified',
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };

  Giddy.prototype.searchUsers = function (options) {
    return axios({
      method: 'GET',
      url: this.endpoint + '/v1/users/search' + '?q=' + encodeURIComponent(options.q),
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      },
      data: undefined
    });
  };
}).call(typeof exports === 'undefined' ? this : exports);

/*eslint-enable */
