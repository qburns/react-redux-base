import React, { Component, PropTypes } from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import Helmet from 'react-helmet';
import * as action from './action';

import styles from './Home.scss';

class Home extends Component {
  // Fetching data method for both server/client side rendering
  static fetchData(dispatch) {
    return Promise.all([
      dispatch(action.fetchDataIfNeeded()),
    ]);
  }

  componentDidMount() {
    const { dispatch } = this.props;

    // Fetching data for client side rendering
    Home.fetchData(dispatch);
  }


  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    return (
      <div className={styles.Home}>
        <Helmet title="Home" />
        This is a fake homepage. Eventually we will have some feeds and stuff here.
        <br />
        <Link to="/protected">Link to top secret stuff that you have to be logged in for</Link>
      </div>
    );
  }
}

Home.propTypes = {
  dispatch: PropTypes.func,
};

const mapStateToProps = state => ({ home: state.get('home'), user: state.get('user') });

export default connect(mapStateToProps)(Home);
