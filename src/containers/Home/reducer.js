/* eslint new-cap:0 */

import { Map } from 'immutable';
import {
  HAVE_ALL_DATA
} from './action';

const initialState = Map({
  readyState: null,
});

export default (state = initialState, action) => {
  switch (action.type) {
    case HAVE_ALL_DATA:
      return state.merge({
        readyState: HAVE_ALL_DATA,
      });
    default:
      return state;
  }
};
