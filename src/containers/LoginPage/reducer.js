/* eslint new-cap:0 */

import { Map, fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';
import { NOT_LOGGED_IN, RETRIEVE_CODE_SUCCESS, RETRIEVE_CODE_FAILURE, LOGIN_FAILURE } from '../../modules/Auth/action';
import { SHOW_PIN_FORM } from './action';

const initialState = Map({
  pageStatus: null,
  code: null,
  err: null,
});

export default (state = initialState, action) => {
  switch (action.type) {
    case NOT_LOGGED_IN:
      return state.merge({
        pageStatus: NOT_LOGGED_IN,
      });
    case RETRIEVE_CODE_SUCCESS:
      return state.merge({
        code: action.payload,
        pageStatus: SHOW_PIN_FORM,
        err: null,
      });
    case RETRIEVE_CODE_FAILURE:
      return state.merge({
        err: fromJS(action.err),
      });
    case LOGIN_FAILURE:
      return state.merge({
        err: fromJS(action.err),
      });
    case LOCATION_CHANGE:
      return initialState;
    default:
      return state;
  }
};
