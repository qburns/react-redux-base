import React, { Component, PropTypes } from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { SHOW_PIN_FORM } from './action';
import LoginForm from '../../components/LoginForm';
import PinForm from '../../components/PinForm';
import { retrieveCodeWithUsername, redirectUser, loginFromPinPage, logoutUser } from '../../modules/Auth/action';

import styles from './LoginPage.scss';

class LoginPage extends Component {

  constructor(props) {
    super(props);
    this.displayLoginIfNeeded = this.displayLoginIfNeeded.bind(this);
  }


  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  /*eslint-disable */

  displayLoginIfNeeded(redirectUrl) {
    const { user, redirectUser, pageStatus} = this.props;
    if (!user) {
      // Right now this is flashing LoginForm for a sec when going to protected routes.
      // Need to setup a better checker before rendering that part so user doesn't see it happening.
      return (pageStatus !== SHOW_PIN_FORM
          ? <LoginForm onSubmit={this.props.retrieveCodeWithUsername} serverError={this.props.err} />
          : <PinForm onSubmit={this.props.loginFromPinPage} serverError={this.props.err} />
        );
    } else {
      redirectUser(redirectUrl);
    }
  }

  /*eslint-enable */
  render() {
    const { redirectAfterLogin } = this.props;
    const redirectUrl = !redirectAfterLogin ? '/' : redirectAfterLogin;
    return (
      <div className={styles.loginPage}>
        <Helmet title="Login to Giddy.io" />
        {this.displayLoginIfNeeded(redirectUrl)}
      </div>
    );
  }
}

LoginPage.propTypes = {
  pageStatus: PropTypes.string,
  err: PropTypes.func,
  redirectUser: PropTypes.func,
  redirectAfterLogin: PropTypes.string,
  user: PropTypes.string,
};

const mapStateToProps = (state, ownProps) => ({
  pageStatus: state.getIn(['loginPage', 'pageStatus']),
  err: state.getIn(['loginPage', 'err']),
  user: state.getIn(['user', 'id']),
  redirectAfterLogin: ownProps.location.query.redirect,
});

export default connect(mapStateToProps, { retrieveCodeWithUsername, redirectUser, loginFromPinPage, logoutUser })(LoginPage);
