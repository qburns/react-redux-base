import React, { PropTypes, Component } from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import config from '../../config';
import Container from 'grommet/components/App';
import Article from 'grommet/components/Article';
import TopHeader from '../../containers/TopHeader';
import { fetchUserIfNeeded } from './action';
import { logoutUser } from '../../modules/Auth/action';

import '../../theme/sanitize.css';

import styles from './App.scss';

class App extends Component {
  static fetchUser(dispatch) {
    return Promise.all([
      dispatch(fetchUserIfNeeded()),
    ]);
  }

  componentDidMount() {
    const { dispatch } = this.props;
    App.fetchUser(dispatch);
  }


  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {

    const { user, dispatch } = this.props;

    return (

      <div className={styles.App}>
        <Container>
          <Article>
            <Helmet {...config.app} />
            <TopHeader />
            {this.props.children}
          </Article>
        </Container>
      </div>
    );
  }
}

App.propTypes = { children: PropTypes.node };

const mapStateToProps = state => ({ user: state.get('user') });

export default connect(mapStateToProps)(App);
