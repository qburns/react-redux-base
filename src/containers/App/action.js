import { getUserFromCookie } from '../../modules/Auth/action';

// Export this function for testing
export const fetchUser = () => (dispatch) => {
  dispatch(getUserFromCookie);
};

// Preventing dobule fetching data
/* istanbul ignore next */
const shouldFetchUser = (state) => {
  /* istanbul ignore next */
  if (__DEV__) return true;

  /* istanbul ignore next */
  const user = state.get('user');

  /* istanbul ignore next */
  if (user.get('id')) return false; // Prevent double fetching user

  /* istanbul ignore next */
  return true;
};

/* istanbul ignore next */
export const fetchUserIfNeeded = () => (dispatch, getState) => {
  if (shouldFetchUser(getState())) {
    /* istanbul ignore next */
    return dispatch(fetchUser());
  }

  /* istanbul ignore next */
  return null;
};
