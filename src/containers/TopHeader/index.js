import React, { Component, PropTypes } from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import Header from 'grommet/components/Header';
import Menu from 'grommet/components/Menu';
import UserIcon from 'grommet/components/icons/base/User';
import { logoutUser } from '../../modules/Auth/action';

import styles from './TopHeader.scss';

class TopHeader extends Component {
  constructor(props) {
    super(props);
    this.showNav = this.showNav.bind(this);
    this.doLogout = this.doLogout.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  doLogout() {
    const { dispatch } = this.props;
    dispatch(logoutUser());
  }

  showNav() {
  const { user } = this.props;

  const username = user.get('userName');

    if (!username) {
      return (
        <Menu direction="row" justify="end" align="center" responsive={false}>
          <Link to="/login">Login</Link>
          <Link to="/signup">Signup</Link>
        </Menu>
      );
    }
    return (
      <Menu direction="row" justify="end" align="center" responsive={false}>
        <Link onClick={this.doLogout}>Logout</Link>
        <p><UserIcon />{username}</p>
      </Menu>
    );
  }

  render() {
    return (
      <Header align="start" className={styles.TopHeader}>
        <Link to="/">
          <img src={require('./assets/logo.png')} className={styles.logo} alt="Giddy" role="presentation"/>
        </Link>
        {this.showNav()}
      </Header>
    );
  }
}

TopHeader.propTypes = {
  dispatch: PropTypes.func,
  dispatch: PropTypes.func,
};

const mapStateToProps = state => ({ user: state.get('user') });

export default connect(mapStateToProps)(TopHeader);
