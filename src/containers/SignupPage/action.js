import Giddy from '../../config';

import {
  AUTO_RETRIEVE_CODE_ATTEMPT,
  AUTO_RETRIEVE_CODE_SUCCESS,
  AUTO_RETRIEVE_CODE_FAILURE,
} from '../../modules/Auth/action';


export const REGISTER_ATTEMPT = 'REGISTER_ATTEMPT';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAILURE = 'REGISTER_FAILURE';

export const checkUsernameExists = (values) => {
  return Giddy.SDK.getAvailableUsername({ username: values.get('username') })
    .catch((err) => {
    console.log(err);
      // eslint-disable-next-line
      if (err == 'Error: Request failed with status code 409') { throw { username: 'That username is taken' } }
    });
};


export const autoRetrieveCode = (username, email) => (dispatch) => {
  dispatch({ type: AUTO_RETRIEVE_CODE_ATTEMPT });

  return Giddy.SDK.loginWithUsername({ username })
    .then((res) => {
      dispatch({ type: AUTO_RETRIEVE_CODE_SUCCESS, payload: { code: res.data.code, email: email } });
    }).catch((err) => {
    console.log(err);
      dispatch({ type: AUTO_RETRIEVE_CODE_FAILURE, err: err.response.data });
    });
};

export const attemptRegister = values => (dispatch) => {
  dispatch({ type: REGISTER_ATTEMPT, values });

  return Giddy.SDK.registerWithEmailAddress(
    { username: values.get('username'),
      email: values.get('email'),
    }).then(() => {
      dispatch({ type: REGISTER_SUCCESS });
      dispatch(autoRetrieveCode(values.get('username'), values.get('email')));
    }).catch((err) => {
      dispatch({ type: REGISTER_FAILURE, err: err.response.data });
    });
};
