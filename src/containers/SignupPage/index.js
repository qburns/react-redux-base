import React, { Component, PropTypes } from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import SignupForm from '../../components/SignupForm';
import PinForm from '../../components/PinForm';
import { attemptRegister } from './action';
import { AUTO_RETRIEVE_CODE, redirectUser, loginFromPinPage } from '../../modules/Auth/action';


import styles from './SignupPage.scss';

class SignupPage extends Component {

  constructor(props) {
    super(props);
    this.displaySignUpIfNeeded = this.displaySignUpIfNeeded.bind(this);
  }


  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }


  /*eslint-disable */
  displaySignUpIfNeeded(redirectUrl) {
    const { user, redirectUser, email} = this.props;
    if (!user) {
      // Right now this is flashing LoginForm for a sec when going to protected routes.
      // Need to setup a better checker before rendering that part so user doesn't see it happening.
      return (
        this.props.pageStatus !== AUTO_RETRIEVE_CODE
          ? <SignupForm onSubmit={this.props.attemptRegister} serverError={this.props.err} />
          : <PinForm onSubmit={this.props.loginFromPinPage} serverError={this.props.err} userEmail={email} />
      );
    } else {
      redirectUser(redirectUrl);
    }
  }
  /*eslint-enable */
  render() {
    const { redirectAfterLogin } = this.props;
    const redirectUrl = !redirectAfterLogin ? '/' : redirectAfterLogin;
    return (
      <div className={styles.signupPage}>
        <Helmet title="Sign up for Giddy.io" />
        {this.displaySignUpIfNeeded(redirectUrl)}
      </div>
    );
  }

}

SignupPage.propTypes = {
  pageStatus: PropTypes.string,
  redirectUser: PropTypes.func,
  redirectAfterLogin: PropTypes.string,
  user: PropTypes.string,
};

const mapStateToProps = (state, ownProps) => ({
  pageStatus: state.getIn(['signupPage', 'pageStatus']),
  err: state.getIn(['signupPage', 'err']),
  email: state.getIn(['signupPage', 'email']),
  user: state.getIn(['user', 'userName']),
  redirectAfterLogin: ownProps.location.query.redirect,
});

export default connect(mapStateToProps, { attemptRegister, redirectUser, loginFromPinPage })(SignupPage);
