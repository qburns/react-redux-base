import Giddy from '../../config';

import {
  AUTO_RETRIEVE_CODE_ATTEMPT,
  AUTO_RETRIEVE_CODE_SUCCESS,
  AUTO_RETRIEVE_CODE_FAILURE,
} from '../../modules/Auth/action';


export const REGISTER_ATTEMPT = 'REGISTER_ATTEMPT';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAILURE = 'REGISTER_FAILURE';

export const checkUsernameExists = (values, dispatch) => {
  return Giddy.SDK.getAvailableUsername(
    {username: values.get('username')})
    .then(() => {
      return true
    })
    .catch((err) => {
      console.log(err);
      throw { username: 'That username is taken' }
    });
};

export const signupValidation = (values) => {

  const username = values.get('username');
  const email = values.get('email');
  const emailRegex = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
  const errors = {};
  if (!username) {
    errors.username = 'Required';
  }
  if (!email) {
    errors.email = 'Required';
  } else if (!emailRegex.test(email)) {
    errors.email = 'Valid email address required.'
  }
  return errors;
};

export const attemptRegister = values => (dispatch) => {
  dispatch({ type: REGISTER_ATTEMPT, values });

  return Giddy.SDK.registerWithEmailAddress(
    { username: values.get('username'),
      email: values.get('email'),
    }).then(() => {
      dispatch({ type: REGISTER_SUCCESS });
      dispatch(autoRetrieveCode(values.get('username')));
    }).catch((err) => {
      dispatch({ type: REGISTER_FAILURE, err: err.response.data });
    });
};


// Export this function for testing
export const fetchData = () => (dispatch) => {
  // At some point we'll need to check whether they're logged in already
  // and tell them they're silly for trying to register again
};


export const autoRetrieveCode = username => (dispatch) => {
  dispatch({ type: AUTO_RETRIEVE_CODE_ATTEMPT });

  return Giddy.SDK.loginWithUsername({ username })
    .then((res) => {
      dispatch({ type: AUTO_RETRIEVE_CODE_SUCCESS, payload: res.data.code });
    }).catch((err) => {
      dispatch({ type: AUTO_RETRIEVE_CODE_FAILURE, err: err.response.data });
    });
};

// Preventing double fetching data
/* istanbul ignore next */
const shouldFetchData = (state) => {
  // In development, we will allow action dispatching
  // or your reducer hot reloading won't updated on the view
  /* istanbul ignore next */
  if (__DEV__) return true;

  /* istanbul ignore next */
  const home = state.get('home');

  /* istanbul ignore next */
  if (home.get('readyState') === USERS_SUCCESS) return false; // Preventing double fetching data

  /* istanbul ignore next */
  return true;
};

/* istanbul ignore next */
export function fetchDataIfNeeded() {
  /* istanbul ignore next */
  return (dispatch, getState) => {
    if (shouldFetchData(getState())) {
      /* istanbul ignore next */
      return dispatch(fetchData());
    }

    /* istanbul ignore next */
    return null;
  };
}

