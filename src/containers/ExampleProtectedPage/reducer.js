/* eslint new-cap:0 */

import { Map, fromJS } from 'immutable';

import {
  REGISTER_ATTEMPT,
  REGISTER_SUCCESS,
  REGISTER_FAILURE,
} from './action';

import {
  AUTO_RETRIEVE_CODE,
  AUTO_RETRIEVE_CODE_ATTEMPT,
  AUTO_RETRIEVE_CODE_SUCCESS,
  AUTO_RETRIEVE_CODE_FAILURE,
} from '../../modules/Auth/action';

const initialState = Map({
  pageStatus: null,
  code: null,
  err: null,
});

export default (state = initialState, action) => {
  switch (action.type) {
    case REGISTER_ATTEMPT:
      return state.merge({
        pageStatus: REGISTER_ATTEMPT,
        err: null,
      });
    case REGISTER_FAILURE:
      return state.merge({
        pageStatus: REGISTER_ATTEMPT,
        err: fromJS(action.err),
      });
    case REGISTER_SUCCESS:
      return state.merge({
        pageStatus: REGISTER_ATTEMPT,
        err: null,
      });
    case AUTO_RETRIEVE_CODE_ATTEMPT:
      return state.merge({
        pageStatus: AUTO_RETRIEVE_CODE,
        err: null,
      });
    case AUTO_RETRIEVE_CODE_SUCCESS:
      return state.merge({
        pageStatus: AUTO_RETRIEVE_CODE,
        err: null,
      });
    case AUTO_RETRIEVE_CODE_FAILURE:
      return state.merge({
        pageStatus: AUTO_RETRIEVE_CODE,
        err: fromJS(action.err),
      });
    default:
      return state;
  }
};
