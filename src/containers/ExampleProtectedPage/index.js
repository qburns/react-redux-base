import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { authenticationRequired } from '../../modules/Auth/action';


import styles from './ExampleProtectedPage.scss';

const ExampleProtectedPage = () => (
  <div className={styles.ExampleProtectedPage}>
    <Helmet title="Super Secretive Page" />
    <h1>You should only see this if you are logged in!</h1>
    <h5>One note: These pages should not be used to store ultra sensitive data. A user, in theory, could modify his client side JS to send a fake username or token along.</h5>
    <h5>When it comes time to fetch sensitive data, we will send along our token to verify.</h5>
    <h5>These protected routes are more about saving us the headache of making calls for a user that isn't logged in, or trying to setup
    authentication checkers on every single page.</h5>
  </div>
);

ExampleProtectedPage.propTypes = {
  username: PropTypes.string,
};

const mapStateToProps = state => ({
  username: state.getIn(['user', 'userName']),
});

/*eslint-disable */
// Note us wrapping authenticationRequired around the Component.
// This is called a "Higher Order Component"
// We will also need to add an onEnter property to the routes.js file for this page to make sure we authenticate
// server side, like when the user tries to navigate directly here.
/*eslint-enable */
export default connect(mapStateToProps)(authenticationRequired(ExampleProtectedPage));
